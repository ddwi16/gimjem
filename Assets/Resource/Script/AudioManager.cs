﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioClip buttonClick, buttonFarming;
    static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        buttonClick = Resources.Load<AudioClip> ("Audio/button_click_1");
        buttonFarming = Resources.Load<AudioClip> ("Audio/nyangkul_1");

        audioSource = GetComponent<AudioSource>(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound(string clip) {
        switch(clip) {
            case "buttonClick" :
                audioSource.PlayOneShot (buttonClick);
                break;
            case "buttonFarm" :
                audioSource.PlayOneShot (buttonFarming);
                break;
        }
    }
}
