﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomMovement : MonoBehaviour
{
    public float accelTime = 2f;
    public float maxSpeed = 5f;
    private Vector2 movement;
    private float timeLeft;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {  
        RandomPos();
    }

    private void FixedUpdate() {
        rb.AddForce(movement * maxSpeed);
    }

    void RandomPos() {
        timeLeft -= Time.deltaTime;
        if(timeLeft <= 0) {
            movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            timeLeft += accelTime;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name != "Farm") {
            accelTime = 0f;
            maxSpeed = 0f;
        }
    }
}
