﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerFarm : MonoBehaviour
{
    public GameObject prefabPeopleInDaFarm;
    public GameObject prefabPeopleInDaHouse;
    public GameObject prefabPeopleInDaQuarantine;
    GameObject instantiateObjectInFarm;
    GameObject instantiateObjectInHouse;
    GameObject instantiateObjectInfected;
    public ManagementPeople counterPeople;
    
    // Start is called before the first frame update
    void Start()
    {
        counterPeople.peopleInHome = 8;
        counterPeople.peopleInFarm = 0;
        counterPeople.peopleQuarantine = 0;
    }

    private void Update() {
        
    }

    //create spawning at farm
    void SpawnPeopleOnClick() {
        Vector3 peoplePos = new Vector3(Random.Range(-9f, 9f), Random.Range(-0.5f, -5f), 0f);
        instantiateObjectInFarm = Instantiate(prefabPeopleInDaFarm, peoplePos, Quaternion.identity);
    }

    //spawn button called
    public void ButtonSpawnPeopleInDaFarm() {
        AudioManager.PlaySound("buttonFarm");
        counterPeople.peopleInHome--;
        counterPeople.peopleInFarm++;
        if(counterPeople.peopleInHome > 0) {
            Invoke("SpawnPeopleOnClick", 0.1f);
        }
        else if(counterPeople.peopleInFarm >= 8 && counterPeople.peopleInHome <= 0) {
            counterPeople.peopleInFarm = 8;
            counterPeople.peopleInHome = 0;
        }
        else
        {
            counterPeople.peopleInHome = 0;
        }

        GameObject objectClone = GameObject.FindGameObjectWithTag("PeopleHome");
        Destroy(objectClone);
    }

    void SpawnPeopleOnHouse() {
        Vector3 peopleStandPos = new Vector3(Random.Range(0.4f, 8.5f), Random.Range(0.2f, 4.5f), 0f);
        instantiateObjectInHouse = Instantiate(prefabPeopleInDaHouse, peopleStandPos, Quaternion.identity);
    }

    public void ButtonSpawnPeopleInDaHouse() {
        AudioManager.PlaySound("buttonClick");
        counterPeople.peopleInHome++;
        counterPeople.peopleInFarm--;
        if(counterPeople.peopleInFarm > 0) {
            Invoke("SpawnPeopleOnHouse", 0.1f);
        }
        else if(counterPeople.peopleInHome >= 8 && counterPeople.peopleInFarm <= 0) {
            counterPeople.peopleInHome = 8;
            counterPeople.peopleInFarm = 0;
        }
        else
        {
            counterPeople.peopleInFarm = 0;
        }

        GameObject objectClone = GameObject.FindGameObjectWithTag("PeopleFarm");
        Destroy(objectClone);
    }
}
