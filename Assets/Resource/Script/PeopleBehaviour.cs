﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleBehaviour : MonoBehaviour
{
    public GameObject flagIsInvected;
    bool isInfected = true;
    public ManagementPeople counterPeople;
    public GameObject peopleQuarantine;

    // Start is called before the first frame update
    void Start()
    {
        counterPeople = GetComponent<ManagementPeople>();
        flagIsInvected.SetActive(!isInfected);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D col) {
        if(col.gameObject.name == "CoronaVirus") {
            flagIsInvected.SetActive(isInfected);
            Debug.Log("Collide");
            Destroy(gameObject, 1f);
            CountInfectPeople();
        }
    }

    void CountInfectPeople() {
        counterPeople.peopleQuarantine++;
        if(counterPeople.peopleQuarantine >= 8 && counterPeople.peopleInFarm <= 0) {
            counterPeople.peopleQuarantine = 8;
            counterPeople.peopleInFarm = 0;
        }
    }
}
